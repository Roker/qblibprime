﻿using Interop.QBFC13;
using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.DataAccess {
    internal static class QBLineItemDao {
        internal static List<LineItem> GetLineItems(IInvoiceRet invoiceRet)
        {
            List<LineItem> lineItems = new List<LineItem>();
            for (int i = 0; i < invoiceRet.ORInvoiceLineRetList.Count; i++)
            {
                IInvoiceLineRet lineRet = invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet;
                if (lineRet == null) continue;

                lineItems.Add(LineItemFromLineRet(lineRet));
            }
            return lineItems;
        }

        internal static LineItem LineItemFromLineRet(IInvoiceLineRet lineRet)
        {
            LineItem lineItem = new LineItem();
            lineItem.LineId = lineRet.TxnLineID.GetValue();

            if(lineRet.ItemRef != null)
            {
                lineItem.ItemId = lineRet.ItemRef.ListID.GetValue();
                lineItem.ItemName = lineRet.ItemRef.FullName.GetValue();
            }

            lineItem.Description = GetDescription(lineRet);
            lineItem.Quantity = GetQuantity(lineRet);
            lineItem.Amount = GetAmount(lineRet);
            lineItem.TaxCodeId = GetTaxCode(lineRet);

            return lineItem;
        }

        private static string GetTaxCode(IInvoiceLineRet lineRet)
        {
            if(lineRet.SalesTaxCodeRef.ListID != null)
            {
                return lineRet.SalesTaxCodeRef.ListID.GetValue();
            }
            return string.Empty;
        }

        private static double GetQuantity(IInvoiceLineRet lineRet)
        {
            if(lineRet.Quantity != null)
            {
                lineRet.Quantity.GetValue();
            }
            return 0;
        }

        private static double GetAmount(IInvoiceLineRet lineRet)
        {
            if (lineRet.Amount != null)
            {
                lineRet.Amount.GetValue();
            }
            return 0;
        }

        private static string GetDescription(IInvoiceLineRet lineRet)
        {
            if(lineRet.Desc != null)
            {
                return lineRet.Desc.GetValue();
            }
            return string.Empty;
        }
    }
}
