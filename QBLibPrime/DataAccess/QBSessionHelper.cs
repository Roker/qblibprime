﻿using Interop.QBFC13;
using Roker.Args.EventArgs;
using System;

namespace QBLibPrime.DataAccess {
    public class QBSessionHelper {

        private object _sessionLock = new object();

        private QBSessionManager _sessionManager;
        private bool _isConnected = false;

        private string _appName;

        //public QBSessionManager SessionManager { get => _sessionManager; set => _sessionManager = value; }

        public QBSessionHelper(string appName)
        {
            _appName = appName;
        }

        public bool ConnectionReady()
        {
            lock (_sessionLock)
            {
                if (_sessionManager == null)
                {
                    try
                    {
                        AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);
                        _sessionManager = new QBSessionManager();
                        // Open the connection and begin a session to QuickBooks
                        _sessionManager.OpenConnection("", _appName);
                        _sessionManager.BeginSession("", ENOpenMode.omDontCare);

                        OnConnectionStatusChanged(true);

                    } catch
                    {
                        OnConnectionFailed();
                        OnConnectionStatusChanged(false);
                        _sessionManager = null;
                        return false;
                    }
                }
                return true;
            }
        }

        private void OnConnectionFailed()
        {
            Console.WriteLine("QBSESSIONHELPER: ConnectionFailed");
            ConnectionFailed?.Invoke(typeof(QBSessionHelper), EventArgs.Empty);
        }

        protected void OnConnectionStatusChanged(bool v) {
            if(_isConnected != v) {
                ConnectionStatusChanged?.Invoke(typeof(QBSessionHelper), new BoolArg(v));
                _isConnected = v;
            }
        }

        public void OnProcessExit(object sender, EventArgs e) {
            if(_sessionManager != null) {
                _sessionManager.EndSession();
                _sessionManager.CloseConnection();
            }
        }

        public IMsgSetRequest NewMsgSetRequest() {
            if (!ConnectionReady()) return null;

            lock (_sessionLock)
            {
                IMsgSetRequest requestMsgSet = _sessionManager.CreateMsgSetRequest("CA", 13, 0);
                requestMsgSet.Attributes.OnError = ENRqOnError.roeContinue;
                return requestMsgSet;
            }

        }

        public T GetResponse<T>(IMsgSetRequest requestMsgSet)
        {
            lock (_sessionLock)
            {
                IMsgSetResponse responseSet = _sessionManager.DoRequests(requestMsgSet);

                IResponse response = responseSet.ResponseList.GetAt(0);
                Console.WriteLine(response.StatusMessage);
                return (T)response.Detail;
            }
        }

        public IMsgSetResponse DoRequests(IMsgSetRequest requestMsgSet) {
            if (!ConnectionReady()) return null;

            lock (_sessionLock)
            {
                return _sessionManager.DoRequests(requestMsgSet);
            }
        }

        public event EventHandler<EventArgs> ConnectionFailed;
        public event EventHandler<BoolArg> ConnectionStatusChanged;
    }
}
