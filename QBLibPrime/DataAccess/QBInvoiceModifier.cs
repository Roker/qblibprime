﻿using Interop.QBFC13;
using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QBLibPrime.DataAccess {
    internal class QBInvoiceModifier {

        internal static async Task<List<Invoice>> Modify(QBSessionHelper session, List<Invoice> invoices, bool modifyLineItems)
        {
            IMsgSetRequest msgSetRequest = session.NewMsgSetRequest();
            foreach(Invoice invoice in invoices)
            {
                AppendInvoiceMod(ref msgSetRequest, invoice, modifyLineItems);
            }

            IMsgSetResponse msgSetResponse = await Task.Run(() => session.DoRequests(msgSetRequest));

            if (msgSetResponse != null)
            {
                if(msgSetResponse.ResponseList.Count != invoices.Count)
                {
                    throw new Exception("Error modifying invoices");
                }
                for (int i=0;i < msgSetResponse.ResponseList.Count;i++)
                {
                    if(msgSetResponse.ResponseList.GetAt(i) is IInvoiceRet invoiceRet)
                    {
                        invoices[i] = InvoiceMapper.InvoiceFromRet(invoiceRet);
                    }
                    else
                    {
                        throw new Exception("Error modifying invoice: " + invoices[i].RefNum);
                    }
                }
            }
            return invoices;
        }

        internal static async Task<Invoice> Modify(QBSessionHelper session, Invoice invoice, bool modifyLineItems)
        {
            IMsgSetRequest msgSetRequest = session.NewMsgSetRequest();
            AppendInvoiceMod(ref msgSetRequest, invoice, modifyLineItems);

            IMsgSetResponse msgSetResponse = await Task.Run(() => session.DoRequests(msgSetRequest));

            if (msgSetResponse != null && msgSetResponse.ResponseList.Count == 1)
            {
                if (msgSetResponse.ResponseList.GetAt(0).Detail is IInvoiceRet ret)
                {
                    return InvoiceMapper.InvoiceFromRet(ret);
                }
            }
            return null;
        }
        private static void AppendInvoiceMod(ref IMsgSetRequest msgSetRequest, Invoice invoice, bool modifyLineItems)
        {
            IInvoiceMod invoiceMod = msgSetRequest.AppendInvoiceModRq();
            invoiceMod.TxnID.SetValue(invoice.TxnId);
            invoiceMod.EditSequence.SetValue(invoice.EditSequence);
            invoiceMod.Memo.SetValue(invoice.Memo);
            invoiceMod.IsToBePrinted.SetValue(invoice.PrintLater);

            if (modifyLineItems)
            {
                foreach(LineItem lineItem in invoice.LineItems)
                {
                    IORInvoiceLineMod lineMod = invoiceMod.ORInvoiceLineModList.Append();
                    lineMod.InvoiceLineMod.TxnLineID.SetValue("-1");

                    if (lineItem.Description != null)
                    {
                        lineMod.InvoiceLineMod.Desc.SetValue(lineItem.Description);
                    }
                    if (lineItem.ItemId != null)
                    {
                        lineMod.InvoiceLineMod.ItemRef.ListID.SetValue(lineItem.ItemId);
                    }
                    if (!string.IsNullOrEmpty(lineItem.TaxCodeId))
                    {
                        lineMod.InvoiceLineMod.SalesTaxCodeRef.ListID.SetValue(lineItem.TaxCodeId);
                    }
                    if (lineItem.Amount.HasValue)
                    {
                        lineMod.InvoiceLineMod.Amount.SetValue((double)lineItem.Amount);
                    }
                    if (lineItem.Quantity.HasValue)
                    {
                        lineMod.InvoiceLineMod.Quantity.SetValue((double)lineItem.Quantity);
                    }
                }
            }
            
        }
    }
}
