﻿using Interop.QBFC13;
using QBLibPrime.Models;
using QBLibPrime.DataAccess.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static QBLibPrime.DataAccess.QBDataHelperMethods;

namespace QBLibPrime.DataAccess {
    public class QBEmployeeDao : QBBaseDao, IQBDao<Employee> {


        public List<Employee> GetAll(RQBSessionHelper session)
        {
            IMsgSetRequest msgSetRequest = session.NewMsgSetRequest();
            AppendEmployeeQueryWithFields(msgSetRequest);

            IEmployeeRetList employeeRetList = session.GetResponse<IEmployeeRetList>(msgSetRequest);
            List<Employee> employees = EmployeesFromRetList(employeeRetList);

            return employees;
        }


        public List<Employee> GetAllByListId(RQBSessionHelper session, List<string> listIds)
        {
            IMsgSetRequest msgSetRequest = session.NewMsgSetRequest();
            IEmployeeQuery employeeQuery = AppendEmployeeQueryWithFields(msgSetRequest);
            employeeQuery.ORListQuery.ListIDList.AddAll(listIds);

            IEmployeeRetList employeeRetList = session.GetResponse<IEmployeeRetList>(msgSetRequest);

            return EmployeesFromRetList(employeeRetList);
        }


        public List<ListItemReference> GetReferenceList(RQBSessionHelper session)
        {
            IMsgSetRequest msgSetRequest = session.NewMsgSetRequest();
            List<ListItemReference> refList = new List<ListItemReference>();

            IEmployeeQuery employeeQuery = AppendEmployeeQuery(msgSetRequest);
            employeeQuery.IncludeRetElementList.Add("ListID");
            employeeQuery.IncludeRetElementList.Add("EditSequence");

            IEmployeeRetList employeeRetList = session.GetResponse<IEmployeeRetList>(msgSetRequest);

            for (int i = 0; i < employeeRetList.Count; i++)
            {
                ListItemReference itemRef = new ListItemReference {
                    ListId = employeeRetList.GetAt(i).ListID.GetValue(),
                    EditSequence = employeeRetList.GetAt(i).EditSequence.GetValue()
                };

                refList.Add(itemRef);
            }
            return refList;
        }

        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        //private methods
        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        private List<Employee> EmployeesFromRetList(IEmployeeRetList employeeRetList)
        {
            List<Employee> employees = new List<Employee>();

            for (int i = 0; i < employeeRetList.Count; i++)
            {
                try
                {
                    employees.Add(EmployeeFromRetList(employeeRetList.GetAt(i)));
                } catch { }
            }
            return employees;
        }

        private Employee EmployeeFromRetList(IEmployeeRet employeeRet)
        {
            string name = employeeRet.Name.GetValue();
            string email = string.Empty;
            try
            {
                email = employeeRet.Email.GetValue();
            } catch { }

            string listID = employeeRet.ListID.GetValue();
            string editSequence = employeeRet.EditSequence.GetValue();

            Employee newEmployee = new Employee();
            newEmployee.Name = name;
            newEmployee.Email = email;
            newEmployee.ListId = listID;
            newEmployee.EditSequence = editSequence;
            newEmployee.WageItems = WageItemsFromRetList(employeeRet);
            return newEmployee;
        }

        private List<WageItem> WageItemsFromRetList(IEmployeeRet employeeRet)
        {
            List<WageItem> wageItems = new List<WageItem>();

            if (employeeRet.EmployeePayrollInfo != null && employeeRet.EmployeePayrollInfo.OREarnings != null && employeeRet.EmployeePayrollInfo.OREarnings.EarningsList != null)
            {
                for (int i = 0; i < employeeRet.EmployeePayrollInfo.OREarnings.EarningsList.Count; i++)
                {
                    WageItem wageItem = new WageItem();
                    try
                    {
                        wageItem.Name = employeeRet
                            .EmployeePayrollInfo.OREarnings.EarningsList.GetAt(i)
                            .PayrollItemWageRef.FullName.GetValue();

                        wageItem.ListId = employeeRet
                            .EmployeePayrollInfo.OREarnings.EarningsList.GetAt(i)
                            .PayrollItemWageRef.ListID.GetValue();

                        wageItem.Rate = employeeRet
                            .EmployeePayrollInfo.OREarnings.EarningsList.GetAt(i).ORRate.Rate.GetValue();
                        wageItems.Add(wageItem);
                    } catch {}
                }
            }

            return wageItems;
        }

        private static IEmployeeQuery AppendEmployeeQuery(IMsgSetRequest requestMsgSet)
        {
            IEmployeeQuery employeeQuery = requestMsgSet.AppendEmployeeQueryRq();
            employeeQuery.ORListQuery.ListFilter.ActiveStatus.SetValue(ENActiveStatus.asActiveOnly);

            return employeeQuery;
        }

        private static IEmployeeQuery AppendEmployeeQueryWithFields(IMsgSetRequest requestMsgSet)
        {
            IEmployeeQuery employeeQuery = AppendEmployeeQuery(requestMsgSet);

            string[] retElementList = { "ListID", "EditSequence", "Name", "Email", "EmployeePayrollInfo" };

            foreach (string s in retElementList)
            {
                employeeQuery.IncludeRetElementList.Add(s);
            }

            return employeeQuery;
        }

        
    }
}
