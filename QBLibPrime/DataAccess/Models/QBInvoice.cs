﻿using Interop.QBFC13;
using System;
using System.Collections.Generic;

namespace QBLib.Quickbooks.Models {
    public class QBInvoice : QBModel {
        private string  _invoiceId;
        private string  _customerName;
        private string  _memo;
        private bool    _printLater;
        private DateTime _dateTime;
        private double  _balanceDue;
        private List<QBLineItem> _lineItems = new List<QBLineItem>();

        public QBInvoice(string listID,string editSequence, string invoiceId, string customerName,
            string memo, bool printLater, DateTime dateTime, double balanceDue, List<QBLineItem> lineItems)
            : base(listID, editSequence)
        {
            _invoiceId = invoiceId;
            _customerName = customerName;
            _memo = memo;
            _printLater = printLater;
            _dateTime = dateTime;
            _balanceDue = balanceDue;
            _lineItems = lineItems;
        }


        //public QBInvoice(string listID, string editSequence) : base(listID, editSequence)
        //{
        //}





        //public QBInvoice(string invoiceId, string quickbooksId, string customerName,
        //    string memo, bool printLater, DateTime dateTime, double balanceDue,
        //    List<QBLineItem> lineItems) : base(invoiceId, quickbooksId)
        //{
        //    _customerName = customerName;
        //    _memo = memo;
        //    _printLater = printLater;
        //    _dateTime = dateTime;
        //    _balanceDue = balanceDue;
        //    _lineItems = lineItems;
        //}

        public string   InvoiceId { get => _invoiceId; }
        public string   CustomerName { get => _customerName; }
        public string   Memo { get => _memo; }
        public bool     PrintLater { get => _printLater; set => _printLater = value; }
        public DateTime DateTime { get => _dateTime; }
        public double   BalanceDue { get => _balanceDue; }
        public List<QBLineItem> LineItems { get => _lineItems; }

        //export invoice to quickbooks
        //public void Export(IInvoiceMod invoiceMod, bool exportLineItems = true) {
        //    invoiceMod.EditSequence.SetValue(EditSequence);
        //    invoiceMod.TxnID.SetValue(_quickbooksId);
        //    invoiceMod.RefNumber.SetValue(_invoiceId);

        //    if(exportLineItems) {
        //        foreach(QBLineItem lineItem in _lineItems) {
        //            IORInvoiceLineMod lineMod = invoiceMod.ORInvoiceLineModList.Append();
        //            lineMod.InvoiceLineMod.TxnLineID.SetValue("-1");

        //            if(lineItem.Description != null) {
        //                lineMod.InvoiceLineMod.Desc.SetValue(lineItem.Description);
        //            }
        //            if(lineItem.ItemId != null) {
        //                lineMod.InvoiceLineMod.ItemRef.ListID.SetValue(lineItem.ItemId);
        //            }
        //            if(lineItem.FullName != null) {
        //                lineMod.InvoiceLineMod.ItemRef.FullName.SetValue(lineItem.FullName);
        //                if(!lineItem.FullName.Equals("SUBTOTAL")) {
        //                    if(lineItem.Amount != 0) {
        //                        lineMod.InvoiceLineMod.Amount.SetValue(lineItem.Amount);
        //                    }
        //                    if(lineItem.Quantity != 0) {
        //                        lineMod.InvoiceLineMod.Quantity.SetValue(lineItem.Quantity);

        //                        if(lineItem.TaxId != null) {
        //                            lineMod.InvoiceLineMod.SalesTaxCodeRef.ListID.SetValue(lineItem.TaxId);
        //                        }
        //                    }
        //                }
        //            }
                    
        //        }
        //    }
            

        //    invoiceMod.IsToBePrinted.SetValue(_printLater);
        //    invoiceMod.Memo.SetValue(_memo);
        //}

    }
}
