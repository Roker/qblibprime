﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLib.Quickbooks.Models {
    public abstract class QBModel {
        private string _listID;
        private string _editSequence;

        protected QBModel(string listID, string editSequence)
        {
            _listID = listID;
            _editSequence = editSequence;
        }

        public string EditSequence { get => _editSequence; set => _editSequence = value; }
        public string ListID { get => _listID; set => _listID = value; }

        public override bool Equals(object obj)
        {
            var model = obj as QBModel;
            return model != null &&
                   _listID == model._listID;
        }

        //generated
        public override int GetHashCode()
        {
            var hashCode = 587516138;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_listID);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_editSequence);
            return hashCode;
        }
    }
}
