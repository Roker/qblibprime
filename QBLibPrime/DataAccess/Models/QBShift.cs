﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QBLib.Quickbooks.Models {
    public class QBShift : QBModel
    {
        private string  _itemRef;
        private string  _notes;
        private short   _duration;
        private DateTime _date;

        public QBShift(string listID, string editSequence, string itemRef, string notes, short duration, DateTime date) : base(listID, editSequence)
        {
            _itemRef = itemRef;
            _notes = notes;
            _duration = duration;
            _date = date;
        }
    }

}
