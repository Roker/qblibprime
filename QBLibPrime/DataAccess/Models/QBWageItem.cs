﻿using System;

namespace QBLib.Quickbooks.Models {
    public class QBWageItem : QBModel {
        private string _name;
        private double _rate;
        private bool _enabled;

        public QBWageItem(string listID, string editSequence,
            string name, double rate, bool enabled) : base(listID, editSequence)
        {
            _name = name;
            _rate = rate;
            _enabled = enabled;
        }
    }
}
