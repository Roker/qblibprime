﻿using System.Collections.Generic;

namespace QBLib.Quickbooks.Models {
    public class QBEmployee : QBModel {
        private string _name;
        private string _email;

        private List<QBWageItem> wageItems = new List<QBWageItem>();

        public QBEmployee(string listID, string editSequence, string name, string email, List<QBWageItem> wageItems) : base(listID, editSequence)
        {
            _name = name;
            _email = email;
            this.wageItems = wageItems;
        }

        //public QBEmployee(string name, string email, List<QBWageItem> wageItems) : base(name, email)
        //{
        //    this.wageItems = wageItems;
        //}

        public string Name { get => _name; }
        public string Email { get => _email; }

        internal void AddWageItem(QBWageItem wageItem) {
            wageItems.Add(wageItem);
        }
    }
}
