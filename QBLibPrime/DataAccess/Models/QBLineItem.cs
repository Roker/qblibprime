﻿using Interop.QBFC13;

namespace QBLib.Quickbooks.Models {
    public class QBLineItem {
        private string _taxId;
        private double _amount;
        private string _lineId;
        private double _quantity;

        private QBItem _item;

        public string LineId { get => _lineId; set => _lineId = value; }
        public string TaxId { get => _taxId; set => _taxId = value; }
        public double Quantity { get => _quantity; set => _quantity = value; }
        public double Amount { get => _amount; set => _amount = value; }

        public QBLineItem(string taxId, double amount, string lineId, double quantity, QBItem item)
        {
            _taxId = taxId;
            _amount = amount;
            _lineId = lineId;
            _quantity = quantity;
            _item = item;
        }



        //public static QBLineItem LineItemByName(string fullName) {
        //    return new QBLineItem(null, null, fullName, null);
        //}

        //public static QBLineItem LineItemById(string itemId) {
        //    return new QBLineItem(null, itemId, null, null);
        //}
    }
}
