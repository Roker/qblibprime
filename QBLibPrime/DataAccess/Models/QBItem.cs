﻿using System.Collections.Generic;

namespace QBLib.Quickbooks.Models {
    public class QBItem : QBModel {
        private string _name;
        private string _description;
        private ItemType _itemType;

        public QBItem(string listID, string editSequence, string name, string description, ItemType itemType) : base(listID, editSequence)
        {
            _name = name;
            _description = description;
            _itemType = itemType;
        }

        public string Name { get => _name; set => _name = value; }
        public string Description { get => _description; set => _description = value; }
        public ItemType ItemType { get => _itemType; set => _itemType = value; }

        public override bool Equals(object obj) {
            if(!(obj is QBItem)) return false;

            QBItem other = (QBItem)obj;
            return _name.Equals(other._name);
        }

        //generated
        public override int GetHashCode()
        {
            var hashCode = -761540558;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_quickbooksId);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_description);
            hashCode = hashCode * -1521134295 + _itemType.GetHashCode();
            return hashCode;
        }
    }

    public enum ItemType{
        SERVICE, DISCOUNT, OTHER_CHARGE, NON_INVENTORY, SALES_TAX
    }
}
