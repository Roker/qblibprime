﻿using System;
using System.Collections.Generic;

namespace QBLib.Quickbooks.Models {
    public class QBCustomer : QBModel, IComparable {
        private string _name;

        public QBCustomer(string listID, string editSequence, string name) : base(listID, editSequence)
        {
            _name = name;
        }

        public string Name { get => _name; }

        public int CompareTo(object obj) {
            return _name.CompareTo(obj);
        }
    }
}
 