﻿using Interop.QBFC13;
using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.DataAccess {
    public interface IQBDao<T> where T : BaseModel {
        List<T> GetAll(RQBSessionHelper session);
        List<T> GetAllByListId(RQBSessionHelper session, List<string> listIds);

        List<QBDataHelperMethods.ListItemReference> GetReferenceList(RQBSessionHelper session);
    }
}
