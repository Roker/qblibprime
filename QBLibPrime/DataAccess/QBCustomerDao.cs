﻿using Interop.QBFC13;
using QBLibPrime.Models;
using QBLibPrime.DataAccess.Extensions;
using System;
using System.Collections.Generic;
using static QBLibPrime.DataAccess.QBDataHelperMethods;

namespace QBLibPrime.DataAccess {
    // Management and flatfile storage of customer information
    public class QBCustomerDao: IQBDao<Customer> {

        public List<Customer> GetAll(RQBSessionHelper session)
        {
            IMsgSetRequest msgSetRequest = session.NewMsgSetRequest();
            ICustomerQuery customerQuery = msgSetRequest.AppendCustomerQueryRq();

            customerQuery.ORCustomerListQuery.CustomerListFilter.ActiveStatus.SetValue(ENActiveStatus.asActiveOnly);

            ICustomerRetList customerRetList = session.GetResponse<ICustomerRetList>(msgSetRequest);

            return CustomersFromRetList(customerRetList);
        }

        public List<Customer> GetAllByListId(RQBSessionHelper session, List<string> listIds)
        {
            IMsgSetRequest msgSetRequest = session.NewMsgSetRequest();

            ICustomerQuery customerQuery = msgSetRequest.AppendCustomerQueryRq();

            customerQuery.ORCustomerListQuery.ListIDList.AddAll(listIds);

            ICustomerRetList customerRetList = session.GetResponse<ICustomerRetList>(msgSetRequest);

            return CustomersFromRetList(customerRetList);
        }

        public List<ListItemReference> GetReferenceList(RQBSessionHelper session)
        {
            IMsgSetRequest msgSetRequest = session.NewMsgSetRequest();
            List<ListItemReference> refList = new List<ListItemReference>();

            ICustomerQuery customerQuery = msgSetRequest.AppendCustomerQueryRq();
            customerQuery.IncludeRetElementList.Add("ListID");
            customerQuery.IncludeRetElementList.Add("EditSequence");

            ICustomerRetList customerRetList = session.GetResponse<ICustomerRetList>(msgSetRequest);

            for (int i = 0; i < customerRetList.Count; i++)
            {
                ListItemReference itemRef = new ListItemReference();

                itemRef.ListId = customerRetList.GetAt(i).ListID.GetValue();
                itemRef.EditSequence = customerRetList.GetAt(i).EditSequence.GetValue();

                refList.Add(itemRef);
            }
            return refList;
        }



        //public static Customer GetCustomer()
        //{
        //    IMsgSetRequest requestMsgSet = QBSessionHelper.RequestMsgSet();
        //    ICustomerQuery customerQuery = requestMsgSet.AppendCustomerQueryRq();
        //    customerQuery.ORCustomerListQuery.CustomerListFilter.ClassFilter.ORClassFilter.ListIDList.Add()
        //}

        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        //private methods
        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        private List<Customer> CustomersFromRetList(ICustomerRetList customerRetList)
        {
            List<Customer> customers = new List<Customer>();

            for (int i = 0; i < customerRetList.Count; i++)
            {
                if (customerRetList.GetAt(i).Name == null) continue;
                if (customerRetList.GetAt(i).ListID == null) continue;

                string name = customerRetList.GetAt(i).Name.GetValue();
                string listID = customerRetList.GetAt(i).ListID.GetValue();
                string editSequence = customerRetList.GetAt(i).EditSequence.GetValue();

                Customer newCustomer = new Customer();
                newCustomer.Name = name;
                newCustomer.ListId = listID;
                newCustomer.EditSequence = editSequence;
                customers.Add(newCustomer);
            }
            return customers;
        }


    }
}
