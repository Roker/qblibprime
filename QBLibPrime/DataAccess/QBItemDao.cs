﻿using Interop.QBFC13;
using QBLibPrime.Models;
using QBLibPrime.DataAccess.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static QBLibPrime.DataAccess.QBDataHelperMethods;

namespace QBLibPrime.DataAccess {
    public class QBItemDao : QBBaseDao, IQBDao<Item> {

        public List<Item> GetAll(RQBSessionHelper session)
        {
            IMsgSetRequest msgSetRequest = session.NewMsgSetRequest();
            IItemQuery itemQuery = msgSetRequest.AppendItemQueryRq();

            itemQuery.ORListQuery.ListFilter.ActiveStatus.SetValue(ENActiveStatus.asActiveOnly);
            IORItemRetList itemRetList = session.GetResponse<IORItemRetList>(msgSetRequest);

            return ItemsFromRetList(itemRetList);
        }

        public List<Item> GetAllByListId(RQBSessionHelper session, List<string> listIds)
        {
            IMsgSetRequest msgSetRequest = session.NewMsgSetRequest();
            IItemQuery itemQuery = msgSetRequest.AppendItemQueryRq();

            itemQuery.ORListQuery.ListIDList.AddAll(listIds);

            IORItemRetList itemRetList = session.GetResponse<IORItemRetList>(msgSetRequest);

            return ItemsFromRetList(itemRetList);
        }

        public List<ListItemReference> GetReferenceList(RQBSessionHelper session)
        {
            List<ListItemReference> refList = new List<ListItemReference>();
            IMsgSetRequest msgSetRequest = session.NewMsgSetRequest();

            IItemQuery itemQuery = msgSetRequest.AppendItemQueryRq();
            itemQuery.IncludeRetElementList.Add("ListID");
            itemQuery.IncludeRetElementList.Add("EditSequence");

            IORItemRetList itemRetList = session.GetResponse<IORItemRetList>(msgSetRequest);

            for (int i = 0; i < itemRetList.Count; i++)
            {
                IORItemRet itemRet = itemRetList.GetAt(i);
                ListItemReference itemRef = new ListItemReference();
                if (itemRet.ItemDiscountRet != null)
                {
                    itemRef.ListId = itemRet.ItemDiscountRet.ListID.GetValue();
                    itemRef.EditSequence = itemRet.ItemDiscountRet.EditSequence.GetValue();
                    refList.Add(itemRef);
                } else if (itemRet.ItemNonInventoryRet != null)
                {
                    itemRef.ListId = itemRet.ItemNonInventoryRet.ListID.GetValue();
                    itemRef.EditSequence = itemRet.ItemNonInventoryRet.EditSequence.GetValue();
                    refList.Add(itemRef);
                } else if (itemRet.ItemOtherChargeRet != null)
                {
                    itemRef.ListId = itemRet.ItemOtherChargeRet.ListID.GetValue();
                    itemRef.EditSequence = itemRet.ItemOtherChargeRet.EditSequence.GetValue();
                    refList.Add(itemRef);
                } else if (itemRet.ItemServiceRet != null)
                {
                    itemRef.ListId = itemRet.ItemServiceRet.ListID.GetValue();
                    itemRef.EditSequence = itemRet.ItemServiceRet.EditSequence.GetValue();
                    refList.Add(itemRef);
                }

            }
            return refList;
        }


    }
}
