﻿using QBLibPrime.Models;

namespace QBLibPrime.DataAccess.Query.ItemRet {
    internal interface ITypedRet {
        string ListID { get; }
        string EditSequence { get; }
        string Name { get; }
        string Description { get; }
        ItemType ItemType { get; }
    }
}