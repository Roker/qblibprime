﻿using Interop.QBFC13;
using QBLibPrime.Models;

namespace QBLibPrime.DataAccess.Query.ItemRet {
    internal class DiscountRet : ITypedRet {
        private IItemDiscountRet _itemDiscountRet;

        public DiscountRet(IItemDiscountRet itemDiscountRet)
        {
            _itemDiscountRet = itemDiscountRet;
        }

        public string ListID {
            get => _itemDiscountRet.ListID.GetValue();
        }
        public string EditSequence {
            get => _itemDiscountRet.EditSequence.GetValue();
        }
        public string Name {
            get => _itemDiscountRet.Name?.GetValue();
        }
        public string Description {
            get => _itemDiscountRet.ItemDesc?.GetValue();
        }

        public ItemType ItemType => ItemType.DISCOUNT;
    }
}