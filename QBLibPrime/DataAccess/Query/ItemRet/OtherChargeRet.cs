﻿using Interop.QBFC13;
using QBLibPrime.Models;

namespace QBLibPrime.DataAccess.Query.ItemRet {
    internal class OtherChargeRet : ITypedRet {
        private IItemOtherChargeRet _itemOtherChargeRet;

        public OtherChargeRet(IItemOtherChargeRet itemOtherChargeRet)
        {
            _itemOtherChargeRet = itemOtherChargeRet;
        }

        public string ListID {
            get => _itemOtherChargeRet.ListID.GetValue();
        }
        public string EditSequence {
            get => _itemOtherChargeRet.EditSequence.GetValue();
        }
        public string Name {
            get => _itemOtherChargeRet.Name?.GetValue();
        }
        public string Description {
            get => _itemOtherChargeRet.ORSalesPurchase?.SalesOrPurchase?.Desc?.GetValue();
        }

        public ItemType ItemType => ItemType.OTHER_CHARGE;
    }
}