﻿using Interop.QBFC13;
using QBLibPrime.Models;

namespace QBLibPrime.DataAccess.Query.ItemRet {
    internal class ServiceRet : ITypedRet {
        private IItemServiceRet _itemServiceRet;

        public ServiceRet(IItemServiceRet itemServiceRet)
        {
            _itemServiceRet = itemServiceRet;
        }

        public string ListID {
            get => _itemServiceRet.ListID.GetValue();
        }
        public string EditSequence {
            get => _itemServiceRet.EditSequence.GetValue();
        }
        public string Name {
            get => _itemServiceRet.Name?.GetValue();
        }
        public string Description {
             get => _itemServiceRet.ORSalesPurchase?.SalesOrPurchase?.Desc?.GetValue();
        }

        public ItemType ItemType => ItemType.SERVICE;
    }
}