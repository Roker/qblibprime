﻿using Interop.QBFC13;
using QBLibPrime.Models;

namespace QBLibPrime.DataAccess.Query.ItemRet {
    internal class NonInventoryRet : ITypedRet {
        private IItemNonInventoryRet _itemNonInventoryRet;

        public NonInventoryRet(IItemNonInventoryRet itemNonInventoryRet)
        {
            _itemNonInventoryRet = itemNonInventoryRet;
        }

        public string ListID {
            get => _itemNonInventoryRet.ListID.GetValue();
        }
        public string EditSequence {
            get => _itemNonInventoryRet.EditSequence.GetValue();
        }
        public string Name {
            get => _itemNonInventoryRet.Name?.GetValue();
        }
        public string Description {
            get => _itemNonInventoryRet.ORSalesPurchase?.SalesOrPurchase?.Desc?.GetValue();
        }

        public ItemType ItemType => ItemType.NON_INVENTORY;
    }
}