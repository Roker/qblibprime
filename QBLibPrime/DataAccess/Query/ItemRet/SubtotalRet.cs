﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interop.QBFC13;
using QBLibPrime.Models;

namespace QBLibPrime.DataAccess.Query.ItemRet {
    internal class SubtotalRet : ITypedRet {
        private IItemSubtotalRet _itemSubtotal;

        public SubtotalRet(IItemSubtotalRet itemSubtotal)
        {
            _itemSubtotal = itemSubtotal;
        }

        public string ListID => _itemSubtotal.ListID.GetValue();

        public string EditSequence => _itemSubtotal.EditSequence.GetValue();

        public string Name => _itemSubtotal.Name?.GetValue();

        public string Description => _itemSubtotal.ItemDesc?.GetValue();

        public ItemType ItemType => ItemType.SUBTOTAL;
    }
}
