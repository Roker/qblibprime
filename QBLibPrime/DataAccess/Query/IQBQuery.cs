﻿using Interop.QBFC13;
using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.DataAccess.Query {
    internal interface IQBQuery<T> where T : BaseModel {
        ENActiveStatus ActiveStatus { get; set; }
        IBSTRList ListIDList { get; }
        IBSTRList IncludeRetElementList { get; }

        List<T> GetResults();
    }
}
