﻿using QBLibPrime.Models;
using System;

namespace QBLibPrime.DataAccess.Query {
    internal class QBQueryFactory {
        internal static IQBQuery<T> Create<T>(QBSessionHelper session) where T : BaseModel
        {
            Type type = typeof(T);
            if(type == typeof(Customer))
            {

                return (IQBQuery<T>)new CustomerQueryAdapter(session);
            }

            if (type == typeof(Item))
            {

                return (IQBQuery<T>)new ItemQuery(session);
            }

            if (type == typeof(TaxCode))
            {

                return (IQBQuery<T>)new TaxCodeQueryAdapter(session);
            }
            Console.WriteLine("QBQueryFactory:");
            throw new InvalidOperationException("NO TYPE");
        }
    }
}