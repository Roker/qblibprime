﻿using Interop.QBFC13;
using QBLibPrime.DataAccess.Extensions;
using QBLibPrime.DataAccess.Query.ItemRet;
using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.DataAccess.Query {
    internal class TaxCodeQueryAdapter : IQBQuery<TaxCode> {
        private QBSessionHelper _session;
        private ISalesTaxCodeQuery _taxQuery;
        private IMsgSetRequest _msgSetRequest;

        internal TaxCodeQueryAdapter(QBSessionHelper session)
        {
            _session = session;

            _msgSetRequest = session.NewMsgSetRequest();
            _taxQuery = _msgSetRequest.AppendSalesTaxCodeQueryRq();
        }

        public IBSTRList ListIDList {
            get => _taxQuery.ORListQuery.ListIDList;
        }

        public ENActiveStatus ActiveStatus {
            get => _taxQuery.ORListQuery.ListFilter.ActiveStatus.GetValue();
            set => _taxQuery.ORListQuery.ListFilter.ActiveStatus.SetValue(value);
        }

        public IBSTRList IncludeRetElementList {
            get => _taxQuery.IncludeRetElementList;
        }

        public List<TaxCode> GetResults()
        {
            ISalesTaxCodeRetList salesTaxCodeRetList = _session.GetResponse<ISalesTaxCodeRetList>(_msgSetRequest);
            return TaxCodesFromRetList(salesTaxCodeRetList);
        }

        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        //private methods
        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        private List<TaxCode> TaxCodesFromRetList(ISalesTaxCodeRetList salesTaxCodeRetList)
        {
            List<TaxCode> taxCodes = new List<TaxCode>();

            for (int i = 0; i < salesTaxCodeRetList.Count; i++)
            {
                try
                {
                    ISalesTaxCodeRet salesTaxRet = salesTaxCodeRetList.GetAt(i);
                    TaxCode taxCode = TaxCodeFromRet(salesTaxRet);
                    taxCodes.Add(taxCode);
                } catch { }
                
            }
            return taxCodes;
        }



        private TaxCode TaxCodeFromRet(ISalesTaxCodeRet salesTaxRet)
        {
            TaxCode taxCode = new TaxCode();
            taxCode.ListId = salesTaxRet.ListID.GetValue();
            taxCode.EditSequence = salesTaxRet.EditSequence.GetValue();
            taxCode.Name = salesTaxRet.Name?.GetValue();
            taxCode.Description = salesTaxRet.Desc?.GetValue();

            return taxCode;
        }

    }
}
