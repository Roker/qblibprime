﻿using Interop.QBFC13;
using QBLibPrime.DataAccess.Extensions;
using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.DataAccess.Query {
    internal class CustomerQueryAdapter : IQBQuery<Customer> {
        private QBSessionHelper _session;
        private ICustomerQuery _customerQuery;
        private IMsgSetRequest _msgSetRequest;

        internal CustomerQueryAdapter(QBSessionHelper session)
        {
            _session = session;

            _msgSetRequest = session.NewMsgSetRequest();
            _customerQuery = _msgSetRequest.AppendCustomerQueryRq();
        }

        public IBSTRList ListIDList {
            get => _customerQuery.ORCustomerListQuery.ListIDList;
        }

        public ENActiveStatus ActiveStatus {
            get => _customerQuery.ORCustomerListQuery.CustomerListFilter.ActiveStatus.GetValue();
            set => _customerQuery.ORCustomerListQuery.CustomerListFilter.ActiveStatus.SetValue(value);
        }

        public IBSTRList IncludeRetElementList {
            get => _customerQuery.IncludeRetElementList;
        }

        public List<Customer> GetResults()
        {
            ICustomerRetList customerRetList = _session.GetResponse<ICustomerRetList>(_msgSetRequest);
            return CustomersFromRetList(customerRetList);
        }

        private List<Customer> CustomersFromRetList(ICustomerRetList customerRetList)
        {
            List<Customer> customers = new List<Customer>();

            for (int i = 0; i < customerRetList.Count; i++)
            {
                if (customerRetList.GetAt(i).Name == null) continue;
                if (customerRetList.GetAt(i).ListID == null) continue;

                string name = customerRetList.GetAt(i).Name.GetValue();
                string listID = customerRetList.GetAt(i).ListID.GetValue();
                string editSequence = customerRetList.GetAt(i).EditSequence.GetValue();

                Customer newCustomer = new Customer();
                newCustomer.Name = name;
                newCustomer.ListId = listID;
                newCustomer.EditSequence = editSequence;
                customers.Add(newCustomer);
            }
            return customers;
        }

    }
}
