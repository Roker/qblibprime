﻿using Interop.QBFC13;
using QBLibPrime.DataAccess.Query.ItemRet;
using QBLibPrime.Models;
using System;
using System.Collections.Generic;

namespace QBLibPrime.DataAccess.Query {
    internal class ItemQuery : IQBQuery<Item> {
        private QBSessionHelper _session;
        private IItemQuery _itemQuery;
        private IMsgSetRequest _msgSetRequest;

        internal ItemQuery(QBSessionHelper session)
        {
            _session = session;

            _msgSetRequest = session.NewMsgSetRequest();
            _itemQuery = _msgSetRequest.AppendItemQueryRq();
        }

        public IBSTRList ListIDList {
            get => _itemQuery.ORListQuery.ListIDList;
        }

        public ENActiveStatus ActiveStatus {
            get => _itemQuery.ORListQuery.ListFilter.ActiveStatus.GetValue();
            set => _itemQuery.ORListQuery.ListFilter.ActiveStatus.SetValue(value);
        }

        public IBSTRList IncludeRetElementList {
            get => _itemQuery.IncludeRetElementList;
        }

        public List<Item> GetResults()
        {
            IORItemRetList itemRetList = _session.GetResponse<IORItemRetList>(_msgSetRequest);


            return ItemsFromRetList(itemRetList);
        }

        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        //private methods
        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        private List<Item> ItemsFromRetList(IORItemRetList itemRetList)
        {
            List<Item> items = new List<Item>();

            for (int i = 0; i < itemRetList?.Count; i++)
            {
                IORItemRet itemRet = itemRetList.GetAt(i);
                try {
                    ITypedRet typedRet = GetTypedRet(itemRet);
                    if(typedRet != null)
                    {
                        items.Add(ItemFromRet(typedRet));
                    }

                } catch (Exception e)
                {
                    Console.WriteLine("Error encountered reading an item from QB.");
                    Console.WriteLine(e.Message);
                }

            }
            return items;
        }



        private ITypedRet GetTypedRet(IORItemRet itemRet)
        {
            switch (itemRet.ortype)
            {
                case ENORItemRet.orirItemDiscountRet:
                    return new DiscountRet(itemRet.ItemDiscountRet);
                case ENORItemRet.orirItemNonInventoryRet:
                    return new NonInventoryRet(itemRet.ItemNonInventoryRet);
                case ENORItemRet.orirItemOtherChargeRet:
                    return new OtherChargeRet(itemRet.ItemOtherChargeRet);
                case ENORItemRet.orirItemServiceRet:
                    return new ServiceRet(itemRet.ItemServiceRet);
                case ENORItemRet.orirItemSubtotalRet:
                    return new SubtotalRet(itemRet.ItemSubtotalRet);
                default:
                    return null;
            }

            
        }

        private Item ItemFromRet(ITypedRet typedRet)
        {
            Item item = new Item();
            item.ListId = typedRet.ListID;
            item.EditSequence = typedRet.EditSequence;
            item.Name = typedRet.Name;
            item.Description = typedRet.Description;
            item.ItemType = typedRet.ItemType;

            if(item.ItemType == ItemType.SUBTOTAL)
            {
                Console.WriteLine(typedRet.Name);
                Console.WriteLine(item.ListId);
            }

            return item;
        }

    }
}
