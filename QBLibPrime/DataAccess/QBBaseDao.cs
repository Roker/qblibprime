﻿using Interop.QBFC13;
using QBLibPrime.DataAccess.Extensions;
using QBLibPrime.DataAccess.Query;
using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using static QBLibPrime.DataAccess.QBDataHelperMethods;

namespace QBLibPrime.DataAccess {
    public class QBBaseDao<T> where T : BaseModel  {
        private QBSessionHelper _session;


        public QBBaseDao(QBSessionHelper session)
        {
            _session = session;
        }

        public T Get(string listId)
        {
            IQBQuery<T> query = QBQueryFactory.Create<T>(_session);

            query.ListIDList.Add(listId);
            List<T> list = query.GetResults();
            if(list?.Count > 0)
            {
                return list[0];
            }
            return null;
        }

        public List<T> GetAll()
        {
            IQBQuery<T> query = QBQueryFactory.Create<T>(_session);
            query.ActiveStatus = ENActiveStatus.asActiveOnly;
            return query.GetResults();
        }

        public List<T> GetAllByListId(List<string> listIds)
        {
            IQBQuery<T> query = QBQueryFactory.Create<T>(_session);

            query.ListIDList.AddAll(listIds);
            return query.GetResults();
        }

        public List<ListItemReference> GetReferenceList()
        {
            List<ListItemReference> refList = new List<ListItemReference>();
            IQBQuery<T> query = QBQueryFactory.Create<T>(_session);

            query.IncludeRetElementList.Add("ListID");
            query.IncludeRetElementList.Add("EditSequence");

            List<T> resultList = query.GetResults();

            foreach(T t in resultList)
            {
                ListItemReference itemRef = new ListItemReference();

                itemRef.ListId = t.ListId;
                itemRef.EditSequence = t.EditSequence;

                refList.Add(itemRef);
            }
            return refList;
        }
    }

    public static class QBDataHelperMethods{

        public struct ListItemReference {
            public string ListId;
            public string EditSequence;
        }
    }
}
