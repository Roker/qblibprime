﻿using Interop.QBFC13;
using QBLibPrime.DataAccess.Query;
using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static QBLibPrime.DataAccess.QBDataHelperMethods;

namespace QBLibPrime.DataAccess.Extensions {
    internal static class IQBQueryExtensions {

        public static List<T> GetAll<T>(this IQBQuery<T> query) where T : BaseModel
        {
            query.ActiveStatus = ENActiveStatus.asActiveOnly;
            return query.GetResults();
        }

        public static List<T> GetAllByListId<T>(this IQBQuery<T> query, List<string> listIds) where T : BaseModel
        {
            query.ListIDList.AddAll(listIds);
            return query.GetResults();
        }

        public static List<ListItemReference> GetReferenceList<T>(IQBQuery<T> query) where T : BaseModel
        {
            List<ListItemReference> refList = new List<ListItemReference>();

            query.IncludeRetElementList.Add("ListID");
            query.IncludeRetElementList.Add("EditSequence");

            List<T> resultList = query.GetResults();

            foreach (T t in resultList)
            {
                ListItemReference itemRef = new ListItemReference();

                itemRef.ListId = t.ListId;
                itemRef.EditSequence = t.EditSequence;

                refList.Add(itemRef);
            }
            return refList;
        }
    }
}
