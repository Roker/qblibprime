﻿using Interop.QBFC13;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.DataAccess.Extensions {
    public static class IORListQueryExtensions {
        public static void AddAll(this IBSTRList list, List<string> ids)
        {
            foreach (string listId in ids)
            {
                list.Add(listId);
            }
        } 
    }
}
