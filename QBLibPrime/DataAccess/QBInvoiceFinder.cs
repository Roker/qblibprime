﻿using Interop.QBFC13;
using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using static QBLibPrime.DataAccess.QBDataHelperMethods;

namespace QBLibPrime.DataAccess {
    public static class QBInvoiceFinder {
        public static List<Invoice> Find(QBSessionHelper session, string customerId, DateTime startDate, DateTime endDate)
        {
            IMsgSetRequest msgSetRequest = session.NewMsgSetRequest();
            IInvoiceQuery invoiceQuery = msgSetRequest.AppendInvoiceQueryRq();
            invoiceQuery.ORInvoiceQuery.InvoiceFilter.ORDateRangeFilter.TxnDateRangeFilter
                            .ORTxnDateRangeFilter.TxnDateFilter.FromTxnDate.SetValue(startDate);
            invoiceQuery.ORInvoiceQuery.InvoiceFilter.ORDateRangeFilter.TxnDateRangeFilter
                            .ORTxnDateRangeFilter.TxnDateFilter.ToTxnDate.SetValue(endDate);
            invoiceQuery.ORInvoiceQuery.InvoiceFilter.EntityFilter.OREntityFilter.ListIDList.Add(customerId);
            invoiceQuery.IncludeLineItems.SetValue(true);

            IInvoiceRetList invoiceRetList = session.GetResponse<IInvoiceRetList>(msgSetRequest);

            return InvoiceMapper.InvoicesFromRetList(invoiceRetList);
        }
    }
}
