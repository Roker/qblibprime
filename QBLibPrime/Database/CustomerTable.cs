﻿using QBLibPrime.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QBLibPrime.Database {
    public class CustomerTable : BaseTable<Customer>, IDataTable<Customer> {
        private static CustomerTable _instance;

        public static CustomerTable Instance { get {
                if (_instance == null)
                {
                    _instance = new CustomerTable();
                }
                return _instance;
            }
        }

        private CustomerTable()
        {
            GetConnection().CreateTableAsync<Customer>().Wait();
        }
    }
}
