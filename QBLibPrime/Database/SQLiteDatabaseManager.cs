﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.Database {
    class SQLiteDatabaseManager {
        private const string DB_NAME = "qb_data.db3";
        private static SQLiteDatabaseManager _instance;

        public static SQLiteDatabaseManager Instance()
        {
                if (_instance == null)
                {
                    _instance = new SQLiteDatabaseManager();
                }
                return _instance;
        }


        private SQLiteConnection _connection;

        public SQLiteConnection Connection { get => _connection; }

        private SQLiteDatabaseManager()
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DB_NAME);
            _connection = new SQLiteConnection(path);
        }
    }
}
