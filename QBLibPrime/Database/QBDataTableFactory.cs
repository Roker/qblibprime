﻿using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.Database {
    internal class QBDataTableFactory {
        internal static IQBDataTable<T> Create<T>() where T : BaseModel
        {
            Type type = typeof(T);
            if (type == typeof(Customer))
            {
                return (IQBDataTable<T>)new QBDataTable<Customer>();
            }
            Console.WriteLine("RQBDataTableFactory:");
            throw new InvalidOperationException("NO TYPE");
        }
    }
}
