﻿using QBLibPrime.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLiteNetExtensions.Attributes;
using System.Reflection;
using System.Linq.Expressions;
using SQLiteNetExtensions.Extensions;
using Roker.Util;

namespace QBLibPrime.Database {
    public class QBDataTable<T> : IQBDataTable<T> where T : BaseModel, new() {
        private string TableName {
            get {
                return ClassUtil.ClassNameFromGenericType(this);
            }
        }

        public QBDataTable()
        {
            //GetConnection().DropTableAsync<T>().Wait();
            GetConnection().CreateTable<T>();
        }


        public bool IsPopulated()
        {
            string sql = string.Format("SELECT * FROM {0} LIMIT 1", TableName);
            List<T> data = GetConnection().Query<T>(sql);

            bool tablePopulated = data != null && data.Count > 0;
            return tablePopulated;
        }

        public bool IsEntryUpToDate(string listID, string editSequence)
        {
            string sql = string.Format("SELECT * FROM {0} WHERE ListId='{1}'", TableName, listID);

            List<T> data = GetConnection().Query<T>(sql);
            bool upToDate = data != null && data.Count == 1 && data[0].EditSequence.Equals(editSequence);
    
            return upToDate;
        }

        public T Get(string listId)
        {
            SQLiteConnection conn = GetConnection();
            try
            {
                return conn.Get<T>(listId);
            } catch
            {
                return null;
            }
        }

        public T First(Expression<Func<T, bool>> expression)
        {
            return GetConnection().Table<T>().First(expression);
        }

        public List<T> GetAllWithChildren()
        {
            return GetConnection().GetAllWithChildren<T>();
        }

        public List<T> GetAllWithChildren(Expression<Func<T,bool>> expression)
        {
            return GetConnection().GetAllWithChildren(expression);
        }

        public void UpdateAllWithChildren(List<T> data)
        {

            GetConnection().InsertOrReplaceAllWithChildren(data);
        }

        public void InsertAllWithChildren(List<T> data)
        {
            GetConnection().InsertAllWithChildren(data);
        }

        protected SQLiteConnection GetConnection()
        {
            return SQLiteDatabaseManager.Instance().Connection;
        }


    }
}
