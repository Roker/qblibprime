﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using QBLibPrime.Models;
using SQLite;

namespace QBLibPrime.Database {
    public interface IQBDataTable<T> where T : BaseModel {
        List<T> GetAllWithChildren();
        List<T> GetAllWithChildren(Expression<Func<T,bool>> expression);
        T First(Expression<Func<T, bool>> expression);
        T Get(string listId);

        void InsertAllWithChildren(List<T> data);
        bool IsEntryUpToDate(string listId, string editSequence);
        bool IsPopulated();
        void UpdateAllWithChildren(List<T> data);
    }
}