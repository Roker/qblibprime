﻿using QBLibPrime.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QBLibPrime.Database {
    public class EmployeeTable : BaseTable<Employee>, IDataTable<Employee> {
        private static EmployeeTable _instance;

        public static EmployeeTable Instance { get {
                if (_instance == null)
                {
                    _instance = new EmployeeTable();
                }
                return _instance;
            }
        }

        private EmployeeTable()
        {
            GetConnection().CreateTableAsync<Employee>().Wait();
        }
    }
}
