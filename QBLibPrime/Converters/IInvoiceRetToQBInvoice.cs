﻿using Interop.QBFC13;
using QBLib.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace QBLibPrime.Converters {
    public class IInvoiceRetToQBInvoice : IConverter<IInvoiceRet, QBInvoice> {
        public QBInvoice Convert(IInvoiceRet invoiceRet)
        {
            string invoiceId;
            string quickbooksId;
            string editSequence;
            string customerName;
            string memo = string.Empty;
            bool printLater;
            DateTime dateTime;
            double balanceDue;

            List<QBLineItem> lineItems = new List<QBLineItem>();


            invoiceId = invoiceRet.RefNumber.GetValue();
            quickbooksId = invoiceRet.TxnID.GetValue();
            editSequence = invoiceRet.EditSequence.GetValue();
            customerName = invoiceRet.CustomerRef.FullName.GetValue();
            printLater = invoiceRet.IsToBePrinted.GetValue();
            dateTime = invoiceRet.TxnDate.GetValue();
            balanceDue = invoiceRet.BalanceRemaining.GetValue();

            try
            {
                memo = invoiceRet.Memo.GetValue();
            } catch
            { }

            for (int i = 0; i < invoiceRet.ORInvoiceLineRetList.Count; i++)
            {
                IInvoiceLineRet lineRet = invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet;
                if (lineRet == null) continue;
                QBLineItem newLineItem = new QBLineItem(lineRet);
                lineItems.Add(newLineItem);
            }

            return new QBInvoice(invoiceId, quickbooksId, editSequence, customerName, memo, printLater, dateTime, balanceDue, lineItems);
        }
    }
}
