﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QBLibPrime.Converters {
    interface IConverter<S, T> {
         T Convert(S source);
    }
}
