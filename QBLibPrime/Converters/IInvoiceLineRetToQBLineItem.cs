﻿using Interop.QBFC13;
using QBLib.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace QBLibPrime.Converters {
    public class IInvoiceLineRetToQBLineItem : IConverter<IInvoiceLineRet, QBLineItem> {
        public QBLineItem Convert(IInvoiceLineRet lineRet)
        {
            string txnLineId = string.Empty;
            string itemId = string.Empty;
            string description = string.Empty;
            string fullName = string.Empty;
            double quantity = 0;
            double amount = 0;
            string taxId = string.Empty;

            lineRet
            try
            {
             lineId = lineRet.TxnLineID.GetValue();
            } catch { }
            try
            {
                itemId = lineRet.ItemRef.ListID.GetValue();
            } catch { }
            try
            {
                description = lineRet.Desc.GetValue();
            } catch { }
            try
            {
                fullName = lineRet.ItemRef.FullName.GetValue();
            } catch { }
            try
            {
                quantity = lineRet.Quantity.GetValue();
            } catch { }
            try
            {
                amount = lineRet.Amount.GetValue();
            } catch { }
            try
            {
                taxId = lineRet.SalesTaxCodeRef.ListID.GetValue();
            } catch { }
            return new QBLineItem(lineId, itemId, fullName, description, quantity, amount, taxId);
        }
    }
}
