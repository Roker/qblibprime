﻿using QBLibPrime.DataAccess;
using QBLibPrime.Managers;
using QBLibPrime.Models;
using Roker.Args.EventArgs;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QBLibPrime.Services {
    public class QBDataService : IQBDataService {
        private QBDataManagerDictionary _dataManagers;
        private QBSessionHelper _session;

        public QBDataService(string appName)
        {
            _session = new QBSessionHelper(appName);
            _session.ConnectionStatusChanged += ConnectionStatusChanged;
            _session.ConnectionFailed += ConnectionFailedChangedEvent;

            _dataManagers = new QBDataManagerDictionary(_session);
        }

        public List<Customer> GetCustomers(Expression<Func<Customer, bool>> expression)
        {
            QBDataManager<Customer> dataManager = _dataManagers.Get<Customer>();

            return dataManager.GetAll(expression);
        }

        public Customer GetCustomer(string QBListId)
        {
            QBDataManager<Customer> dataManager = _dataManagers.Get<Customer>();

            return dataManager.Get(QBListId);
        }

        public List<Item> GetItems(Expression<Func<Item, bool>> expression)
        {
            QBDataManager<Item> dataManager = _dataManagers.Get<Item>();

            return dataManager.GetAll(expression);
        }

        public Item GetItem(string QBListId)
        {
            QBDataManager<Item> dataManager = _dataManagers.Get<Item>();

            return dataManager.Get(QBListId);
        }

        public List<Item> GetDiscounts()
        {
            QBDataManager<Item> dataManager = _dataManagers.Get<Item>();

            return dataManager.GetAll((Item x) => x.ItemType == ItemType.DISCOUNT);
        }

        public List<TaxCode> GetTaxCodes()
        {
            QBDataManager<TaxCode> dataManager = _dataManagers.Get<TaxCode>();

            return dataManager.GetAll();
        }

        public List<Invoice> FindInvoices(string customerListId, DateTime startDate, DateTime endDate)
        {
            return QBInvoiceFinder.Find(_session, customerListId, startDate, endDate);
        }

        public async Task<List<Invoice>> UpdateInvoices(List<Invoice> invoices, bool modifyLineItems)
        {
            List<Invoice> retInvoices = new List<Invoice>();
            foreach(Invoice invoice in invoices)
            {
                retInvoices.Add(await QBInvoiceModifier.Modify(_session, invoice, modifyLineItems));
            }
            return retInvoices;
        }

        public void Init<T>() where T : BaseModel, new()
        {
            _dataManagers.Get<T>();
        }



        public event EventHandler<BoolArg> ConnectionStatusChanged;
        public event EventHandler<EventArgs> ConnectionFailed;


        //private void ConnectionStatusChangedEvent(object sender, BoolArg e)
        //{
        //    ConnectionStatusChanged?.Invoke(typeof(RQBDataService), e);
        //}

        private void ConnectionFailedChangedEvent(object sender, EventArgs e)
        {
            ConnectionFailed?.Invoke(typeof(QBDataService), e);
        }


    }
}
