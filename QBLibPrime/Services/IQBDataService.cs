﻿using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QBLibPrime.Services {
    public interface IQBDataService {
        List<Customer> GetCustomers(Expression<Func<Customer, bool>> expression = null);
        List<Item> GetItems(Expression<Func<Item, bool>> expression = null);
        List<Item> GetDiscounts();
        List<TaxCode> GetTaxCodes();

        Customer GetCustomer(string QBListId);
        Item GetItem(string QBListId);

        void Init<T>() where T : BaseModel, new();

        //event EventHandler<BoolArg> ConnectionStatusChanged;
        //event EventHandler<EventArgs> ConnectionFailed;

        List<Invoice> FindInvoices(string customerListId, DateTime startDate, DateTime endDate);
        Task<List<Invoice>> UpdateInvoices(List<Invoice> invoices, bool modifyLineItems);
    }
}
