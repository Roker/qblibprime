﻿using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QBLibPrime.Services {
    public class DesignQBDataService : IQBDataService {
        //public event EventHandler<BoolArg> ConnectionStatusChanged;
        //public event EventHandler<EventArgs> ConnectionFailed;

        public DesignQBDataService() {

        }

        public Customer GetCustomer(string QBListId)
        {
            return null;
        }

        public List<Customer> GetCustomers(Expression<Func<Customer, bool>> expression = null)
        {
            return null;
        }

        public List<Item> GetDiscounts()
        {
            return null;
        }

        public Item GetItem(string QBListId)
        {
            return null;
        }

        public List<Item> GetItems(Expression<Func<Item, bool>> expression = null)
        {
            return null;
        }

        public List<TaxCode> GetTaxCodes()
        {
            List<TaxCode> taxCodes = new List<TaxCode> {
                new TaxCode {
                    Name = "G"
                },
                new TaxCode {
                    Name = "S"
                }
            };
            return taxCodes;
        }

        public void Init<T>() where T : BaseModel, new()
        {
            //
        }

        public List<Invoice> FindInvoices(string customerListId, DateTime startDate, DateTime endDate)
        {
            return null;
        }


        public Task<List<Invoice>> UpdateInvoices(List<Invoice> invoices, bool modifyLineItems)
        {
            throw new NotImplementedException();
        }
    }
}
