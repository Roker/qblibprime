﻿using Interop.QBFC13;
using QBLibPrime.DataAccess;
using QBLibPrime.Database;
using QBLibPrime.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using static QBLibPrime.DataAccess.QBDataHelperMethods;

namespace QBLibPrime.Managers {
    public class QBDataManager<T> where T : BaseModel {
        public bool IsInit { get; private set; }

        private QBBaseDao<T> _qbDao;
        private IQBDataTable<T> _table;
        private QBSessionHelper _qBSessionHelper;

        public QBDataManager(QBSessionHelper qbSessionHelper, IQBDataTable<T> table)
        {
            _qBSessionHelper = qbSessionHelper;
            _qbDao = new QBBaseDao<T>(qbSessionHelper);
            _table = table;
            Init();
        }


        private void Init()
        {
            if (!_qBSessionHelper.ConnectionReady()) throw new InvalidOperationException(); //connection not ready

            if (_table.IsPopulated())
            {
                IsInit = UpdateData(); //table populated, update data selectively
            } else
            {
                IsInit = ImportData(); //table not populated, import all data
            }
        }

        public List<T> GetAll(Expression<Func<T, bool>> expression)
        {
            return _table.GetAllWithChildren(expression);
        }

        public List<T> GetAll()
        {
            return _table.GetAllWithChildren();
        }

        public T First(Expression<Func<T, bool>> expression)
        {
            return _table.First(expression);
        }

        public T Get(string listId)
        {
            T t = _table.Get(listId);


            if (t == null)
            {
                t = _qbDao.Get(listId);
            }

            if(t == null)
            {
                throw new InvalidOperationException();
            }
            return t;
        }

        private bool ImportData()
        {
            Console.WriteLine("Importing...");

            List<T> data = _qbDao.GetAll();

            _table.InsertAllWithChildren(data);
            Console.WriteLine("Done.");
            return true;
        }

        private bool UpdateData()
        {
            Console.WriteLine("Updating...");
            List<ListItemReference> itemRefs = GetItemRefs();

            List<string> needUpdateList = new List<string>();
            foreach (ListItemReference itemRef in itemRefs)
            {
                if (!_table.IsEntryUpToDate(itemRef.ListId, itemRef.EditSequence))
                {
                    needUpdateList.Add(itemRef.ListId);
                }
            }

            Console.WriteLine(needUpdateList.Count + " records need updating");

            if (needUpdateList.Count > 0)
            {
                List<T> data = _qbDao.GetAllByListId(needUpdateList);

                _table.UpdateAllWithChildren(data);
            }
            return true;
        }

        private List<ListItemReference> GetItemRefs()
        {
            IMsgSetRequest msgSetRequest = _qBSessionHelper.NewMsgSetRequest();
            List<ListItemReference> itemRefs = _qbDao.GetReferenceList();
            return itemRefs;
        }
    }
}
