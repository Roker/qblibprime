﻿using QBLibPrime.Models;
using QBLibPrime.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QBLibPrime.Database;

namespace QBLibPrime.Managers {
    internal class QBDataManagerDictionary {

        private object _lock = new object();
        private Dictionary<string, object> _dataManagers = new Dictionary<string, object>();
        private QBSessionHelper _session;

        public QBDataManagerDictionary(QBSessionHelper session)
        {
            _session = session;
        }

        internal void Add<T>(QBDataManager<T> value) where T : BaseModel {
            string key = typeof(T).Name;
            if (_dataManagers.ContainsKey(key)){
                _dataManagers.Remove(key);
            }
            _dataManagers.Add(key, value);
        }


        internal QBDataManager<T> Get<T>() where T : BaseModel, new()
        {
            string key = typeof(T).Name;

            if (_dataManagers.ContainsKey(key))
            {
                return (QBDataManager<T>)_dataManagers[key];
            } else
            {

                QBDataManager<T> manager = new QBDataManager<T>(_session, new QBDataTable<T>());
                _dataManagers.Add(key, manager);
                return manager;
            }
        }
    }
}
