﻿using QBLibPrime.Database;
using QBLibPrime.Models;
using QBLibPrime.Quickbooks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static QBLibPrime.Quickbooks.QBCustomerHelper;
using static QBLibPrime.Quickbooks.QBDataHelper;

namespace QBLibPrime.Managers {
    public class EmployeeManager {
        //public static async Task InitAsync()
        //{
        //    if (await CustomerTable.Instance.IsTablePopulated())
        //    {
        //        await UpdateCustomers();
        //    } else {
        //        await ImportCustomers();
        //    }
        //}

        public static async Task ImportEmployeesAsync()
        {
            try
            {
                List<Employee> employees = QBEmployeeHelper.GetEmployees(QBSessionHelper.Instance.RequestMsgSet());

            } catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            //await CustomerTable.Instance.InsertAllAsync(customers);
        }

        //private static async Task UpdateCustomers()
        //{
        //    List<ListItemReference> itemRefs = GetReferenceList(QBSessionHelper.Instance.RequestMsgSet());
        //    List<string> needUpdate = new List<string>();
        //    foreach(ListItemReference itemRef in itemRefs)
        //    {
        //        if (!await CustomerTable.Instance.IsCustomerUpdatedAsync(itemRef.ListId, itemRef.EditSequence))
        //        {
        //            needUpdate.Add(itemRef.ListId);
        //        }
        //    }


        //    List<Customer> customers = QBCustomerHelper.GetCustomersByListId(QBSessionHelper.Instance.RequestMsgSet(), needUpdate);

        //    await CustomerTable.Instance.UpdateAllAsync(customers);
        //}
    }
}
