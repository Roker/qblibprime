﻿using QBLibPrime.Database;
using QBLibPrime.Models;
using QBLibPrime.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.Managers {
    public static class QBDataManagerFactory {
        internal static QBDataManager<T> Create<T>(QBSessionHelper session) where T : BaseModel, new()
        {
            Type type = typeof(T);
            IQBDataTable<T> table;
            if (type == typeof(Customer))
            {
                table = (IQBDataTable<T>)new QBDataTable<T>();
            }else if (type == typeof(Item))
            {
                table = (IQBDataTable<T>)new QBDataTable<Item>();
            } else if (type == typeof(Item))
            {
                table = (IQBDataTable<T>)new QBDataTable<TaxCode>();
            } else
            {
                throw new InvalidOperationException("NO TYPE");

            }
            return new QBDataManager<T>(session, table);

        }
    }
}
