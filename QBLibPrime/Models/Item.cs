﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.Models {
    public class Item : BaseModel {
        public string Description { get; set; }
        public ItemType ItemType { get; set; }


        public override string ToString()
        {
            return Name;
        }
    }

    public enum ItemType {
        SERVICE, DISCOUNT, OTHER_CHARGE, NON_INVENTORY, TAX_CODE, SUBTOTAL
    }


    public static class ItemTypeExtensions {
        public static string ToString(this ItemType itemType)
        {
            return Enum.GetName(typeof(ItemTypeExtensions), itemType);
        }
    }
}
