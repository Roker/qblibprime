﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace QBLibPrime.Models {
    public class Employee : BaseModel {
        public string Email { get; set; }

        [TextBlob("WageItemsBlobbed")]
        public List<WageItem> WageItems { get; set; }
        public string WageItemsBlobbed { get; set; } // serialized WageItems
    }
}
