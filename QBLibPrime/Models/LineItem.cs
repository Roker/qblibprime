﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.Models {
    public class LineItem {
        public string LineId { get; set; }
        public string ItemId { get; set; }
        public string Description { get; set; }
        public string ItemName { get; set; }
        public double? Quantity { get; set; }
        public double? Amount { get; set; }
        public string TaxCodeId { get; set; }
    }
}
