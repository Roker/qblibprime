﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace QBLibPrime.Models {
    public class EmployeeWageItem : BaseModel {
        [ForeignKey(typeof(Employee))] public int EmployeeListId { get; set; }
        [ForeignKey(typeof(WageItem))] public int WageItemListId { get; set; }
    }
}
