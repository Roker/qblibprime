﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.Models {
    public class Invoice {
        public string TxnId { get; internal set; }
        public string EditSequence { get; internal set; }
        public string RefNum { get; set; }
        public string Memo { get; set; }
        public bool PrintLater { get; set; }
        public DateTime Date { get; set; }
        public double BalanceDue { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }

        public List<LineItem> LineItems { get; set; }
    }
}
