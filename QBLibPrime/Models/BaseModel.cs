﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace QBLibPrime.Models {
    public abstract class BaseModel {

        public BaseModel() {}
        [PrimaryKey]
        public string ListId { get; set; }

        public string EditSequence { get; set; }
        public string Name { get; set; }
    }
}
