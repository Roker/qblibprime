﻿using Interop.QBFC13;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBLibPrime.Models
{
    public static class InvoiceMapper
    {
        internal static List<Invoice> InvoicesFromRetList(IInvoiceRetList invoiceRetList)
        {
            List<Invoice> invoices = new List<Invoice>();
            if (invoiceRetList != null)
            {
                for (int i = 0; i < invoiceRetList.Count; i++)
                {
                    IInvoiceRet invoiceRet = invoiceRetList.GetAt(i);

                    invoices.Add(InvoiceFromRet(invoiceRet));
                }
            }

            return invoices;
        }

        internal static Invoice InvoiceFromRet(IInvoiceRet invoiceRet)
        {
            Invoice invoice = new Invoice();
            invoice.TxnId = invoiceRet.TxnID.GetValue();
            invoice.EditSequence = invoiceRet.EditSequence.GetValue();
            invoice.RefNum = invoiceRet.RefNumber.GetValue();
            invoice.CustomerId = invoiceRet.CustomerRef.ListID.GetValue();
            invoice.CustomerName = invoiceRet.CustomerRef.FullName.GetValue();
            invoice.PrintLater = invoiceRet.IsToBePrinted.GetValue();
            invoice.Date = invoiceRet.TxnDate.GetValue();
            invoice.BalanceDue = invoiceRet.BalanceRemaining.GetValue();
            invoice.Memo = MemoFromRet(invoiceRet);
            invoice.LineItems = LineItemsFromInvoiceRet(invoiceRet);

            return invoice;
        }

        internal static List<LineItem> LineItemsFromInvoiceRet(IInvoiceRet invoiceRet)
        {
            List<LineItem> lineItems = new List<LineItem>();
            for (int i = 0; i < invoiceRet.ORInvoiceLineRetList.Count; i++)
            {
                IInvoiceLineRet lineRet = invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet;
                if (lineRet == null) continue;

                lineItems.Add(LineItemFromLineRet(lineRet));
            }
            return lineItems;
        }

        internal static LineItem LineItemFromLineRet(IInvoiceLineRet lineRet)
        {
            LineItem lineItem = new LineItem();
            lineItem.LineId = lineRet.TxnLineID.GetValue();

            if (lineRet.ItemRef != null)
            {
                lineItem.ItemId = lineRet.ItemRef.ListID.GetValue();
                lineItem.ItemName = lineRet.ItemRef.FullName.GetValue();
                lineItem.Amount = GetAmount(lineRet);
                lineItem.Quantity = QuantityFromLineRet(lineRet);
                lineItem.TaxCodeId = TaxCodeFromLineRet(lineRet);
            }

            lineItem.Description = GetDescription(lineRet);

            return lineItem;
        }

        internal static string MemoFromRet(IInvoiceRet invoiceRet)
        {
            if (invoiceRet.Memo != null)
            {
                return invoiceRet.Memo.GetValue();
            }
            return string.Empty;
        }

        internal static string TaxCodeFromLineRet(IInvoiceLineRet lineRet)
        {
            if (lineRet.SalesTaxCodeRef?.ListID != null)
            {
                return lineRet.SalesTaxCodeRef.ListID.GetValue();
            }
            return string.Empty;
        }

        private static double? QuantityFromLineRet(IInvoiceLineRet lineRet)
        {
            if (lineRet.Quantity != null)
            {
                return lineRet.Quantity.GetValue();
            }
            return null;
        }

        private static double? GetAmount(IInvoiceLineRet lineRet)
        {
            if (lineRet.Amount != null && lineRet.ORRate != null)
            {
                return lineRet.Amount.GetValue();
            }
            return null;
        }

        private static string GetDescription(IInvoiceLineRet lineRet)
        {
            if (lineRet.Desc != null)
            {
                return lineRet.Desc.GetValue();
            }
            return string.Empty;
        }
    }
}
