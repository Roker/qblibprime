﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace QBLibPrime.Models {
    public class WageItem : BaseModel {
        public double Rate { get; set; }
    }
}
