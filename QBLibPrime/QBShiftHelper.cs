﻿using Interop.QBFC13;
using System.Collections.Generic;

namespace QBLib.data {
    public class QBShiftHelper
    {
        public static string[] ExportShifts(QBShift[] shifts) {
            List<string> statusMessages = new List<string>();

            IMsgSetRequest msgSetRequest = QBSessionHelper.RequestMsgSet();

            foreach(QBShift shift in shifts) {
                ITimeTrackingAdd timeTrackingAdd = msgSetRequest.AppendTimeTrackingAddRq();

                timeTrackingAdd.EntityRef.ListID.SetValue(shift.qbId);
                timeTrackingAdd.PayrollItemWageRef.FullName.SetValue(shift.itemRef);
                timeTrackingAdd.Duration.SetValue(0, shift.duration, 0, false);
                timeTrackingAdd.TxnDate.SetValue(shift.date);
                timeTrackingAdd.BillableStatus.SetValue(ENBillableStatus.bsNotBillable);
                timeTrackingAdd.Notes.SetValue(shift.notes);
            }

            IMsgSetResponse response = QBSessionHelper.GetInstance().DoRequests(msgSetRequest);
            for(int i = 0; i < response.ResponseList.Count; i++) {
                IResponse r = response.ResponseList.GetAt(i);
                statusMessages.Add(r.StatusCode.ToString() + "|" + r.StatusMessage.ToString() + "|" + shifts[i].qbId + "|" + shifts[i].date + "|" + shifts[i].itemRef);
            }

            return statusMessages.ToArray();
        }
    }
}
